export default class FileService {
  constructor(file) {
    this._fileReder = new FileReader();
    this._file = file;
  }

  get result() {
    return this._fileReder.result;
  }

  async fileToBase64() {
    return new Promise((resolve, reject) => {
      this._fileReder.readAsDataURL(this._file);
      this._fileReder.onload = () => resolve();
      this._fileReder.onerror = (error) => reject(error);
    });
  }
}
