import axios from "axios";
export default class HttpService {
  async testAxios() {
    const result = await axios("https://jsonplaceholder.typicode.com/posts/");
    return result;
  }

  async uploadFiles(file) {
    const query = `
      mutation{
        ImageUpload(base64:{base64: "${file.base64}", filename: "${file.filename}", token: "${file.token}"}){
            url,
        }
      }
    
    
  `;
    const config = {
      method: "post",
      url: "http://127.0.0.1:3000/graphql",
      headers: {
        "Content-Type": "application/json",
      },
      data: { query },
    };
    const result = await axios(config);
    return result;
  }
}
